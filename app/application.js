/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function cleanImportedHtml(html) {
    return html.replace('module.exports = "', '')
        .replace('";', '')
        .replace(/  /g, '')
        .replace(/\n/g, '');
}

const Util = {
    cachedFiles: {},
    $on: (target, event, handler) => {
        return target.addEventListener(event, handler);
    },
    forObjectKeys: (object, callback) => {
        for (let key in object) {
            callback(object[key], key);
        }
    },
    getFile: (name) => {
        return cleanImportedHtml(__webpack_require__(3)("./" + name + '.html'))
    },
    bindValue: (element, content, lookForValue) => {
        const contentProperty = element.getAttribute('reference');
        const value = content[contentProperty] ? content[contentProperty].value : '';

        if (!contentProperty) {
            return;
        }

        if (element.type === 'text') {
            element.value = value;
        } else {
            element.textContent = lookForValue ? value : content[contentProperty];
        }
    }
};
/* harmony export (immutable) */ __webpack_exports__["a"] = Util;


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Events; });
const Events = {
    controlTabClick: (tabs) => {
        if (tabs && Events.controller) {
            Events.controller.controlTabsChange(tabs);
        }
    },
    infoDataChange: (model, replace) => {
        if (model && Events.controller) {
            Events.controller.infoModelChange(model, replace);
        }
    },
    init: (controller) => {
        Events.controller = controller;
    }
};



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__events__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__controller__ = __webpack_require__(22);






class App {
  constructor() {
    const model = new __WEBPACK_IMPORTED_MODULE_3__model__["a" /* default */]();
    const view = new __WEBPACK_IMPORTED_MODULE_2__view__["a" /* default */]();

    this.controller = new __WEBPACK_IMPORTED_MODULE_4__controller__["a" /* default */](model, view);
    __WEBPACK_IMPORTED_MODULE_1__events__["a" /* Events */].init(this.controller);
  };
}

const app = new App();

const setView = () => {
  app.controller.setView(document.location.hash);
};

__WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(window, 'load', setView);
__WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(window, 'hashchange', setView);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./content-tab-1.html": 4,
	"./content-tab-2.html": 5,
	"./content-tab-3.html": 6,
	"./content-tab-about.html": 7,
	"./content-tab-settings.html": 8,
	"./control-tabs.html": 9,
	"./edit-form.html": 10,
	"./edit-popup.html": 11,
	"./followers.html": 12,
	"./info-profile-image.html": 13,
	"./log-out.html": 14,
	"./rating-reviews.html": 15,
	"./upload-image.html": 16
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 3;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=content-title>1</div>\";";

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=content-title>2</div>\";";

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=content-title>3</div>\";";

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=content-tab-about><div class=content-title>About<ion-icon bind-click reference=all class=toggle-all-fields id=toggle-all-fields name=create></ion-icon><div class=edit-form__buttons><div bind-click reference=save>Save</div><div bind-click reference=cancel>Cancel</div></div></div><div id=edit-form class=edit-form><div id=profile-name class=edit-form__item><span bind-data reference=name class=edit-form__item__value>Jessica Parker</span><div class=edit-form__input><div class=edit-form__input-placeholder>First Name</div><input type=text bind-change bind-data reference=firstName /></div><div class=edit-form__input><div class=edit-form__input-placeholder>Last Name</div><input type=text bind-change bind-data reference=lastName /></div><ion-icon bind-click reference=name name=create></ion-icon></div><div class=edit-form__item><ion-icon name=pin></ion-icon><span bind-data reference=website class=edit-form__item__value>www.seller.com</span><div class=edit-form__input><div class=edit-form__input-placeholder>Webiste</div><input type=text bind-change bind-data reference=website /></div><ion-icon bind-click reference=website name=create></ion-icon></div><div class=edit-form__item><ion-icon name=call></ion-icon><span bind-data reference=phone class=edit-form__item__value>(949)325-68594</span><div class=edit-form__input><div class=edit-form__input-placeholder>Phone number</div><input type=text bind-change bind-data reference=phone /></div><ion-icon bind-click reference=phone name=create></ion-icon></div><div class=edit-form__item><ion-icon name=home></ion-icon><span bind-data reference=location class=edit-form__item__value>Newport Beach, CA</span><div class=edit-form__input><div class=edit-form__input-placeholder>City, State & Zip</div><input type=text bind-change bind-data reference=location /></div><ion-icon bind-click reference=location name=create></ion-icon></div></div></div>\";";

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=content-title>Settings</div>\";";

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div id=control-tabs class=control-tabs> <div bind-click reference=about class=control-tabs__item>About</div> <div bind-click reference=settings class=control-tabs__item>Settings</div> <div bind-click reference=1 class=control-tabs__item>Option 1</div> <div bind-click reference=2 class=control-tabs__item>Option 2</div> <div bind-click reference=3 class=control-tabs__item>Option 3</div> </div>\";";

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div id=edit-form class=edit-form><div id=profile-name class=edit-form__item><span bind-data reference=name class=edit-form__item__value>Jessica Parker</span><div class=edit-form__input><div class=edit-form__input-placeholder>First Name</div><input type=text bind-change bind-data reference=firstName /></div><div class=edit-form__input><div class=edit-form__input-placeholder>Last Name</div><input type=text bind-change bind-data reference=lastName /></div><ion-icon bind-click reference=name name=create></ion-icon></div><div class=edit-form__item><ion-icon name=pin></ion-icon><span bind-data reference=website class=edit-form__item__value>www.seller.com</span><div class=edit-form__input><div class=edit-form__input-placeholder>Webiste</div><input type=text bind-change bind-data reference=website /></div><ion-icon bind-click reference=website name=create></ion-icon></div><div class=edit-form__item><ion-icon name=call></ion-icon><span bind-data reference=phone class=edit-form__item__value>(949)325-68594</span><div class=edit-form__input><div class=edit-form__input-placeholder>Phone number</div><input type=text bind-change bind-data reference=phone /></div><ion-icon bind-click reference=phone name=create></ion-icon></div><div class=edit-form__item><ion-icon name=home></ion-icon><span bind-data reference=location class=edit-form__item__value>Newport Beach, CA</span><div class=edit-form__input><div class=edit-form__input-placeholder>City, State & Zip</div><input type=text bind-change bind-data reference=location /></div><ion-icon bind-click reference=location name=create></ion-icon></div></div>\";";

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div id=edit-popup class=edit-popup> <div class=edit-form__input> <div bind-data reference=placeholder class=edit-form__input-placeholder>Placeholder</div> <input type=text bind-change bind-data reference=value /> </div> <div class=edit-popup__buttons> <div bind-click reference=save>Save</div> <div bind-click reference=cancel>Cancel</div> </div> </div>\";";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=followers> <ion-icon name=add-circle></ion-icon> <span id=followers__count>6</span> <span>Followers</span> </div>\";";

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=info-profile-image><div class=image-container style=background-image:url(&quot;./assets/profile_image.jpg&quot;)></div></div>\";";

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=log-out> Log out </div>\";";

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=\\\"rating-reviews flex\\\"> <div class=rating-reviews__stars> <ion-icon name=star></ion-icon> <ion-icon name=star></ion-icon> <ion-icon name=star></ion-icon> <ion-icon name=star></ion-icon> <ion-icon name=star></ion-icon> </div> <div class=rating-reviews__count> <span id=rating-reviews__count>6</span> <span>Reviews</span> </div> </div>\";";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "module.exports = \"<div class=upload-image__cover> <ion-icon name=camera></ion-icon> Upload Cover Image </div>\";";

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__util__ = __webpack_require__(0);



class View {
    constructor() {
        this.el = document.getElementById('App');
    };
    controlTabsChange(tabs, data) {
        let selectedTab = {};

        __WEBPACK_IMPORTED_MODULE_1__util__["a" /* Util */].forObjectKeys(tabs, (tab) => {
            if (tab.selected) {
                selectedTab = tab;
            }
        });

        __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */].switchTabContent(selectedTab);
        __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */].updateDataBindings(data);
    };
    infoModelChange(model, replace) {
        __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */].updateInfoModel(model, replace);
    };

    render(data) {
        this.el.innerHTML = __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */].render();
        __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */].loadApp(data);
    };
}
/* harmony export (immutable) */ __webpack_exports__["a"] = View;


/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return App; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__control_tabs__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__edit_form__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util__ = __webpack_require__(0);




function checkForMobileDevice(target) {
    let isMobile = false;

    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) isMobile = true;})(navigator.userAgent||navigator.vendor||window.opera);

    if (isMobile) {
        let widthOfApp = 0;
        if (window.outerHeight){
            widthOfApp = window.outerWidth;
        }
        else {
            widthOfApp = document.body.clientWidth;
        }

        target.classList.add('mobile-version');
        target.style.width = String(widthOfApp) + 'px';
    }
}

const App = {
    target: {},
    render: () => {
        return '<div id="application-wrapper" class="application-wrapper"><div class="controls-info-wrapper content-box-shadow flex-wrap full-flex-box"><div class="full-flex-box padding-20 justify-end"><div class="log-out">Log out</div></div><div class="full-flex-box justify-center"><div class="upload-image__cover"><ion-icon name="camera"></ion-icon>Upload Cover Image</div></div><div class="info flex-wrap"><div class="info-profile-image"><div class="image-container" style="background-image: url(&quot;./assets/profile_image.jpg&quot;)"></div></div><div class="flex flex-column"><div bind-data reference="name" class="info__name">Jessica Parker</div><div class="info__additional"><ion-icon name="pin"></ion-icon><span bind-data reference="location">Newport Beach, CA</span></div><div class="info__additional"><ion-icon name="call"></ion-icon><span bind-data reference="phone">(949)325-68594</span></div></div></div><div class="rating-reviews flex"><div class="rating-reviews__stars"><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon><ion-icon name="star"></ion-icon></div><div class="rating-reviews__count"><span id="rating-reviews__count">6</span><span>Reviews</span></div></div><div class="followers"><ion-icon name="add-circle"></ion-icon><span id="followers__count">6</span><span>Followers</span></div><div id="control-tabs" class="control-tabs"><div bind-click reference="about" class="control-tabs__item">About</div><div bind-click reference="settings" class="control-tabs__item">Settings</div><div bind-click reference="1" class="control-tabs__item">Option 1</div><div bind-click reference="2" class="control-tabs__item">Option 2</div><div bind-click reference="3" class="control-tabs__item">Option 3</div></div></div><div class="content-edit-wrapper content-box-shadow flex-wrap full-flex-box"><div dynamic-html="switchTabContent"><div class="content-tab-about"><div class="content-title">About<ion-icon bind-click reference="all" class="toggle-all-fields" id="toggle-all-fields" name="create"></ion-icon><div class="edit-form__buttons"><div bind-click reference="save">Save</div><div bind-click reference="cancel">Cancel</div></div></div><div id="edit-form" class="edit-form"><div id="profile-name" class="edit-form__item"><span bind-data reference="name" class="edit-form__item__value">Jessica Parker</span><div class="edit-form__input"><div class="edit-form__input-placeholder">First Name</div><input type="text" bind-change bind-data reference="firstName" /></div><div class="edit-form__input"><div class="edit-form__input-placeholder">Last Name</div><input type="text" bind-change bind-data reference="lastName" /></div><ion-icon bind-click reference="name" name="create"></ion-icon></div><div class="edit-form__item"><ion-icon name="pin"></ion-icon><span bind-data reference="website" class="edit-form__item__value">www.seller.com</span><div class="edit-form__input"><div class="edit-form__input-placeholder">Webiste</div><input type="text" bind-change bind-data reference="website" /></div><ion-icon bind-click reference="website"name="create"></ion-icon></div><div class="edit-form__item"><ion-icon name="call"></ion-icon><span bind-data reference="phone" class="edit-form__item__value">(949)325-68594</span><div class="edit-form__input"><div class="edit-form__input-placeholder">Phone number</div><input type="text" bind-change bind-data reference="phone" /></div><ion-icon bind-click reference="phone"name="create"></ion-icon></div><div class="edit-form__item"><ion-icon name="home"></ion-icon><span bind-data reference="location" class="edit-form__item__value">Newport Beach, CA</span><div class="edit-form__input"><div class="edit-form__input-placeholder">City, State & Zip</div><input type="text" bind-change bind-data reference="location" /></div><ion-icon bind-click reference="location"name="create"></ion-icon></div></div></div></div></div></div>';
    },
    loadApp: (data) => {
        App.target = document.getElementById('application-wrapper');
        checkForMobileDevice(App.target);
        const dataBindElements = App.target.querySelectorAll('[bind-data]');

        __WEBPACK_IMPORTED_MODULE_0__control_tabs__["a" /* ControlTabs */].init(data.controlTabs);
        __WEBPACK_IMPORTED_MODULE_1__edit_form__["a" /* EditForm */].init(data.info);

        if (dataBindElements && dataBindElements.length) {
            dataBindElements.forEach((element) => {
                __WEBPACK_IMPORTED_MODULE_2__util__["a" /* Util */].bindValue(element, data.info, true);
            });
        }
    },
    switchTabContent: (tab) => {
        const contentFileHTML = __WEBPACK_IMPORTED_MODULE_2__util__["a" /* Util */].getFile('content-tab-' + tab.id);
        const dynamicContentWrapper = App.target.querySelectorAll('[dynamic-html="switchTabContent"]');

        for (let iterator = 0; iterator < dynamicContentWrapper.length; iterator++) {
            dynamicContentWrapper[iterator].innerHTML = contentFileHTML;
        }
    },
    updateDataBindings: (data) => {
        __WEBPACK_IMPORTED_MODULE_1__edit_form__["a" /* EditForm */].init(data.info);
    },
    updateInfoModel: (model, replace) => {
        if (replace) {
            __WEBPACK_IMPORTED_MODULE_2__util__["a" /* Util */].forObjectKeys(model, (value, key) => {
                const reference = key;
                const newValue = value.value;
                const elementsForUpdating = App.target.querySelectorAll('[bind-data][reference=' + reference + ']');

                if (elementsForUpdating) {
                    elementsForUpdating.forEach((element) => {
                        element.textContent = newValue;
                    });
                }
            });

            return;
        }
        const reference = model.type;
        const newValue = model.value;
        const elementsForUpdating = App.target.querySelectorAll('[bind-data][reference=' + reference + ']');

        if (elementsForUpdating) {
            elementsForUpdating.forEach((element) => {
                element.textContent = newValue;
            });
        }
    }
};


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ControlTabs; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__events__ = __webpack_require__(1);



const ControlTabs = {
    state: {},
    target: {},
    selectTab: (tab, shouldUpdateData) => {
        let currentlySelectedTabElement = ControlTabs.target.querySelector('.control-tabs__item--selected');
        let tabElement = ControlTabs.target.querySelector('[reference="'+ tab.id +'"]');

        if (currentlySelectedTabElement) {
            currentlySelectedTabElement.classList.remove('control-tabs__item--selected');
        }
        if (tabElement) {
            tabElement.classList.add('control-tabs__item--selected');
        }
        if (shouldUpdateData) {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].forObjectKeys(ControlTabs.state.tabs, (tab) => {
                tab.selected = false;
            });
            ControlTabs.state.tabs[tab.id].selected = true;

            __WEBPACK_IMPORTED_MODULE_1__events__["a" /* Events */].controlTabClick(ControlTabs.state.tabs);
        }
    },
    observe: () => {
        let clickableElements = ControlTabs.target.querySelectorAll('[bind-click]');

        if (clickableElements) {
            clickableElements.forEach((element) => {
                __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'click', (event) => {
                    const tabId = event.currentTarget.getAttribute('reference');

                    __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].forObjectKeys(ControlTabs.state.tabs, (tab) => {
                        if (tab.id === tabId) {
                            ControlTabs.selectTab(tab, true);
                        }
                    });
                });
            });
        }
    },
    init: (controlTabs) => {
        ControlTabs.target = document.getElementById('control-tabs');

        if (ControlTabs.state && !ControlTabs.state.tabs) {
            ControlTabs.state.tabs = controlTabs;
        }

        __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].forObjectKeys(ControlTabs.state.tabs, (tab) => {
            if (tab.selected) {
                ControlTabs.selectTab(tab);
            }
        });

        ControlTabs.observe();
    }
};



/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__events__ = __webpack_require__(1);



function getPopupContent(inputId, model) {
    let popupContent = {};

    switch (inputId) {
        case 'location':
            popupContent = {
                placeholder: 'City. state & Zip',
                value: model.location
            };
            break;
        case 'phone':
            popupContent = {
                placeholder: 'Phone number',
                value: model.phone
            };

            break;
        case 'website':
            popupContent = {
                placeholder: 'Website',
                value: model.website
            };

            break;
        case 'name':
            popupContent = {
                placeholder: 'Full name',
                value: model.name
            };

            break;
    }

    return popupContent;
}
function destroyPopup(emptyModel) {
    const popupWrappers = EditForm.target.querySelectorAll('.edit-popup-wrapper');

    if (popupWrappers && popupWrappers.length) {
        popupWrappers.forEach((element) => {
            element.parentNode.removeChild(element);
        });
    }
    if (emptyModel) {
        EditForm.state.popupModel = {};
    }

    EditForm.state.popupActive = false;
}
function closeForm() {
    EditForm.target.parentNode.classList.remove('toggle-all-inputs');
    EditForm.state.formModel = {};
}
function toggleForm(wrapper) {
    EditForm.state.formModel = {
        name: EditForm.state.model.name,
        firstName: EditForm.state.model.firstName,
        lastName: EditForm.state.model.lastName,
        location: EditForm.state.model.location,
        phone: EditForm.state.model.phone,
        website: EditForm.state.model.website
    };
    const clickBindElements = wrapper.querySelectorAll('[bind-click]');
    const changeBindElements = EditForm.target.querySelectorAll('[bind-change]');

    if (clickBindElements) {
        clickBindElements.forEach((element) => {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'click', (event) => {
                event.stopPropagation();
                event.preventDefault();
                const action = event.currentTarget.getAttribute('reference');

                if (action && action === 'save') {
                    if (EditForm.state.formModel.firstName) {
                        EditForm.state.formModel.name.value = EditForm.state.formModel.firstName.value;
                    }
                    if (EditForm.state.formModel.lastName) {
                        EditForm.state.formModel.name.value = EditForm.state.formModel.firstName ? EditForm.state.formModel.firstName.value + ' ' + EditForm.state.formModel.lastName.value : EditForm.state.formModel.lastName.value;
                    }

                    __WEBPACK_IMPORTED_MODULE_1__events__["a" /* Events */].infoDataChange(EditForm.state.formModel, true);
                }

                closeForm();
            });
        });
    }
    if (changeBindElements) {
        changeBindElements.forEach((element) => {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'keyup', (event) => {
                EditForm.state.formModel[event.currentTarget.getAttribute('reference')].value = event.currentTarget.value;
            });
        });
    }
}
function togglePopup(wrapper, content, html) {
    let dataBindElements = [];
    let clickBindElements = [];
    let changeBindElements = [];
    let popupWrapper = document.createElement('div');

    destroyPopup();

    EditForm.state.popupActive = true;
    popupWrapper.className = 'edit-popup-wrapper content-box-shadow';
    wrapper.appendChild(popupWrapper);

    wrapper.querySelector('.edit-popup-wrapper').innerHTML = html;
    dataBindElements = wrapper.querySelectorAll('[bind-data]');
    clickBindElements = wrapper.querySelectorAll('[bind-click]');
    changeBindElements = wrapper.querySelectorAll('[bind-change]');

    if (dataBindElements) {
        dataBindElements.forEach((element) => {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].bindValue(element, content);
        });
    }
    if (changeBindElements) {
        EditForm.state.popupModel.value = EditForm.state.model[EditForm.state.popupModel.type].value;

        changeBindElements.forEach((element) => {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'keyup', (event) => {
                EditForm.state.popupModel.value = event.currentTarget.value;
            });
        });
    }
    if (clickBindElements) {
        clickBindElements.forEach((element) => {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'click', (event) => {
                event.stopPropagation();
                event.preventDefault();
                const action = event.currentTarget.getAttribute('reference');

                if (action && action === 'save') {
                    __WEBPACK_IMPORTED_MODULE_1__events__["a" /* Events */].infoDataChange(EditForm.state.popupModel);
                }

                destroyPopup(true);
            });
        });
    }
}

const EditForm = {
    state: {
        popupModel: {}
    },
    target: {},
    observe: () => {
        let clickableElements = EditForm.target.querySelectorAll('[bind-click]');
        let toggleAllInputs = document.querySelector('#toggle-all-fields');

        if (clickableElements) {
            clickableElements.forEach((element) => {
                __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(element, 'click', (event) => {
                    if (!EditForm.state.popupActive) {
                        const inputId = event.currentTarget.getAttribute('reference');
                        const popupContent = getPopupContent(inputId, EditForm.state.model);
                        const popupHtml = __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].getFile('edit-popup');

                        EditForm.state.popupModel.type = inputId;

                        togglePopup(event.currentTarget, popupContent, popupHtml);
                    } else {
                        event.stopPropagation();
                        event.preventDefault();
                    }

                });
            });
        }
        if (toggleAllInputs) {
            __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].$on(toggleAllInputs, 'click', () => {
                EditForm.target.parentNode.classList.add('toggle-all-inputs');

                toggleForm(toggleAllInputs.parentNode.querySelector('.edit-form__buttons'));
            });
        }
    },
    init: (infoModel) => {
        EditForm.target = document.getElementById('edit-form');

        if (!EditForm.state || !EditForm.target || !infoModel) {
            return;
        }

        EditForm.state.model = infoModel;

        if (infoModel.name && infoModel.name.value) {
            const nameParts = infoModel.name.value.split(" ");

            EditForm.state.model.firstName = {
                value: nameParts[0] ? nameParts[0] : ''
            };
            EditForm.state.model.lastName = {
                value: nameParts[1] ? nameParts[1] : ''
            };
        }
        const dataBindElements = EditForm.target.querySelectorAll('[bind-data]');

        if (dataBindElements && dataBindElements.length) {
            dataBindElements.forEach((element) => {
                __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].bindValue(element, infoModel, true);
            });
        }

        EditForm.observe();
    }
};



/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util__ = __webpack_require__(0);


class Model {
    constructor() {
        this.data = {};

        this.data.controlTabs = {
            about: {
                title: 'About',
                selected: true,
                id: 'about'
            },
            settings: {
                title: 'Settings',
                selected: false,
                id: 'settings'
            },
            1: {
                title: 'Option1',
                selected: false,
                id: '1'
            },
            2: {
                title: 'Option2',
                selected: false,
                id: '2'
            },
            3: {
                title: 'Option3',
                selected: false,
                id: '3'
            }
        };
        this.data.info = {
            name: {
                value: 'Jessica Parker'
            },
            location: {
                value: 'Newport Beach, CA'
            },
            phone: {
                value: '(949)325-68594'
            },
            website: {
                value: 'www.seller.com'
            }
        };
    };
    setControlTabs(tabs) {
        this.data.controlTabs = tabs;
    };
    setInfo(model, replace) {
        if (replace) {
            this.data.info = model;

            return;
        }
        this.data.info[model.type].value = model.value;
    };
    setModelData(data) {
        __WEBPACK_IMPORTED_MODULE_0__util__["a" /* Util */].forObjectKeys(data.info, (value, key) => {
            if (this.data.info[key]) {
                this.data.info[key] = value;
            }
        });
    };
    getControlTabs() {
        return this.data.controlTabs;
    };

    getData() {
        return this.data;
    };
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Model;


/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Controller {

    constructor(model, view) {
        this.model = model;
        this.view = view;
    };
    setView(hash) {
        // For implementation of custom page redirection.
        const validURL = /^#\/[\d]{2}\/[\d]{4}$/.test(hash);

        if (validURL) {
            const matches = hash.match(/^#\/([\d]{2})\/([\d]{4})$/);
            // @TODO Continue from here <* REDIRECTION *>
        }

        this.render();
    };
    controlTabsChange(tabs) {
        this.model.setControlTabs(tabs);
        this.view.controlTabsChange(tabs, this.model.getData());

        this.updateLocalStorage(this.model.getData());
    };
    infoModelChange(model, replace) {
        this.model.setInfo(model, replace);
        this.view.infoModelChange(model, replace);

        this.updateLocalStorage(this.model.getData());
    };
    updateLocalStorage(data) {
        if (localStorage) {
            localStorage.setItem('socialNetworkData', JSON.stringify(data));
        }
    };
    checkLocalStorage() {
        if (localStorage && localStorage.getItem('socialNetworkData')) {
            this.model.setModelData(JSON.parse(localStorage.getItem('socialNetworkData')));

            return true;
        }

        return false;
    };

    render() {
        const existingData = this.checkLocalStorage();

        this.view.render(this.model.getData());
        if (existingData) {
            this.controlTabsChange(this.model.getControlTabs());
        }
    };
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Controller;


/***/ })
/******/ ]);